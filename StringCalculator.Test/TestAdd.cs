﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace StringCalculator.Test
{
    [TestFixture]
    public class UnitTest1
    {
        Calculator _calculator = null;

        [OneTimeSetUp]
        public void Init()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Adding_THEN_ReturnZero()
        {
            var expcted = 0;
            var actual = _calculator.Add(string.Empty);

            NUnit.Framework.Assert.AreEqual(expcted, actual);
        }

        [Test]
        public void GIVEN_OneNumber_WHEN_Adding_THEN_ReturnThatNumber()
        {
            var expcted = 1;
            var actual = _calculator.Add("1");

            NUnit.Framework.Assert.AreEqual(expcted, actual);
        }

        [Test]
        public void GIVEN_TwoNumbers_WHEN_Adding_THEN_ReturnSumO()
        {
            var expected = 3;
            var actual = _calculator.Add("1,2");

            NUnit.Framework.Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NewLineDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 6;
            var actaul = _calculator.Add("1,2\n3");

            NUnit.Framework.Assert.AreEqual(expected, actaul);
        }

        [Test]
        public void GIVEN_MultipleNumbers_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("1,2\n3,4");

            NUnit.Framework.Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_CustomDelimiters_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 6;
            var actual = _calculator.Add("//;\n1;2;3");

            NUnit.Framework.Assert.AreEqual(expected, actual);
        }
    }
}
