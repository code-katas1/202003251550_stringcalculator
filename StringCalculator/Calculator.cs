﻿using System.Collections.Generic;

namespace StringCalculator
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string[] delimiters = GetDelimiter(numbers);
            List<int> numbersArray = GetNumbers(numbers, delimiters);

            return GetSum(numbersArray);
        }

        public string[] GetDelimiter(string numbers)
        {
            var customDelimiterId = "//";
            var delimiterSeparator = "\n";

            if (numbers.StartsWith("//"))
            {
                return new[] { numbers.Substring(numbers.IndexOf("//") + customDelimiterId.Length, numbers.IndexOf("\n") - delimiterSeparator.Length - 1) };
            }

            return new[] { delimiterSeparator, "," };
        }

        public List<int> GetNumbers(string numbers, string[] delimiters)
        {
            string[] stringNumbersArray = numbers.Split(delimiters, System.StringSplitOptions.RemoveEmptyEntries);
            List<int> numbersArray = new List<int>();

            foreach (var number in stringNumbersArray)
            {
                if (int.TryParse(number, out int num))
                {
                    numbersArray.Add(num);
                }
            }

            return numbersArray;
        }

        public int GetSum(List<int> numbersArray)
        {
            int sum = 0;
            foreach (var number in numbersArray)
            {
                sum += number;
            }

            return sum;
        }
    }
}
